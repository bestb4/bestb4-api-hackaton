class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.float :price
      t.float :quantity
      t.integer :best_before
      t.date :date
      t.float :lat
      t.float :lon
      t.string :image_path
      t.float :distance

      t.timestamps
    end
  end
end
