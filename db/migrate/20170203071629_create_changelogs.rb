class CreateChangelogs < ActiveRecord::Migration[5.0]
  def change
    create_table :changelogs do |t|
      t.integer :best_before
      t.float :price
      t.float :quantity
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
