require "rails_helper"

RSpec.describe ChangelogsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/changelogs").to route_to("changelogs#index")
    end

    it "routes to #new" do
      expect(:get => "/changelogs/new").to route_to("changelogs#new")
    end

    it "routes to #show" do
      expect(:get => "/changelogs/1").to route_to("changelogs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/changelogs/1/edit").to route_to("changelogs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/changelogs").to route_to("changelogs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/changelogs/1").to route_to("changelogs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/changelogs/1").to route_to("changelogs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/changelogs/1").to route_to("changelogs#destroy", :id => "1")
    end

  end
end
