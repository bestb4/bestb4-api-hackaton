class ChangelogsController < ApplicationController
  before_action :set_changelog, only: [:show, :update, :destroy]

  # GET /changelogs
  def index
    @changelogs = Changelog.all

    render json: @changelogs
  end

  # GET /changelogs/1
  def show
    render json: @changelog
  end

  # POST /changelogs
  def create
    @changelog = Changelog.new(changelog_params)

    if @changelog.save
      render json: @changelog, status: :created, location: @changelog
    else
      render json: @changelog.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /changelogs/1
  def update
    if @changelog.update(changelog_params)
      render json: @changelog
    else
      render json: @changelog.errors, status: :unprocessable_entity
    end
  end

  # DELETE /changelogs/1
  def destroy
    @changelog.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_changelog
      @changelog = Changelog.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def changelog_params
      params.require(:changelog).permit(:best_before, :price, :quantity, :product_id)
    end
end
