class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  # GET /products
  def index
    @products = Product.all()
    if(params.key?("lat") and params.key?("lon"))
      @products = getclose(params)
    end
    if(params.key?("search"))
      search_result = Array.new
      @products.each do |product|
        if(product.name =~ /(.*)#{params["search"]}(.*)/i)
          search_result << product
        end
      end
      @products = search_result
    end
    valid_products = Array.new
    @products.each do |product|
      best_before = product.calculate_best_before_in_days
      product.best_before = best_before
      valid_products.push(product)
    end

    valid_products.sort! { |a, b|  a.distance <=> b.distance }
    render json: valid_products
  end

  # GET /products/1
  def show
    best_before = @product.calculate_best_before_in_days
    @product.best_before = best_before
    render json: @product
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      changelog = Changelog.new(product_params)
      @product.changelogs.push(changelog)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  end

  def getclose(params)
    latitude = params["lat"].to_f
    longitude = params["lon"].to_f
    closest_products = Array.new
    max_range = 10
    if params.key?("max_range") 
      max_range = params["max_range"].to_f
    end
    products = Product.all()
    for product in products
      product.distance = product.getDistance(latitude, longitude)
      if(product.distance < max_range)
        closest_products << product
      end
    end

    return closest_products
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:name, :description, :price, :quantity, :date, :user_id, :lat, :lon)
    end
end
