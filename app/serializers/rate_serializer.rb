class RateSerializer < ActiveModel::Serializer
  attributes :id, :comment, :positive
  has_one :user
  has_one :product
end
