class ChangelogSerializer < ActiveModel::Serializer
  attributes :id, :best_before, :price, :quantity
  has_one :product
end
