class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :lat, :lon, :price, :quantity, :date, :best_before, :user, :rates, :changelogs, :image_path, :distance
end
