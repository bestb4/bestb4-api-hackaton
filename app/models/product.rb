require 'haversine'

class Product < ApplicationRecord
  validates :name, presence: true
  validates :quantity, presence: true
  validates :price, presence: true
  validates :date, presence: true

  belongs_to :user
  has_many :rates
  has_many :changelogs

  def calculate_best_before_in_days
    self.date.mjd - Date.today.mjd
  end

  def getDistance(latitude, longitude)
    Haversine.distance(latitude, longitude, self.lat, self.lon);
  end

end
